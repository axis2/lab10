#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <unistd.h>
#include <time.h>

#include "errhandle.h"


#define LINES_COUNT                 10
#define PARENT_PREFIX               "[parent]"
#define CHILD_PREFIX                "[child]"


#define MUTEX_COUNT     3u
#define DONT_LOCK       MUTEX_COUNT


unsigned nextLockIndex(const unsigned index) {
    return (index + 1) % MUTEX_COUNT;
}


struct st_PrintArguments {
    char const *const prefix;
    unsigned const lock_first;
    pthread_mutex_t *const locks;
};
typedef struct st_PrintArguments PrintArguments_t;


#define lock(PMutex)      lock_((PMutex), __FUNCTION__, __LINE__)


int lock_(
        pthread_mutex_t *p_mutex,
        char const *function,
        int line);


#define must_unlock(PMutex) must_unlock_((PMutex), __FUNCTION__, __LINE__)


void must_unlock_(
        pthread_mutex_t *p_mutex,
        char const *function,
        int line);


void *printLines(void *v_args) {
    if (NULL == v_args) {
        ferrorfln("Unexpected argument value: null");
        return NULL;
    }
    const PrintArguments_t *const args = (PrintArguments_t *) v_args;
    if (NULL == args->prefix) {
        ferrorfln("No prefix provided");
        return NULL;
    }

    pthread_mutex_t *const locks = args->locks;

    int errcode;

    unsigned lock_index = args->lock_first;
    if (DONT_LOCK != lock_index) {
        errcode = lock(&locks[lock_index]);
        if (NO_ERROR != errcode) {
            errorfln(
                    "%s [first lock] Cannot acquire lock[%d]",
                    args->prefix, nextLockIndex(lock_index));
            if (EDEADLOCK == errcode) {
                must_unlock(&locks[lock_index]);
            }
            return NULL;
        }
    } else {
        lock_index = 0;
    }

    for (int i = 0; i < LINES_COUNT; i += 1) {
        errcode = lock(&locks[nextLockIndex(lock_index)]);
        if (NO_ERROR != errcode) {
            errorfln(
                    "%s [iter %d] Cannot acquire lock[%d]",
                    args->prefix, i, nextLockIndex(lock_index));
            if (EDEADLOCK == errcode) {
                must_unlock(&locks[nextLockIndex(lock_index)]);
            }
            must_unlock(&locks[lock_index]);
            return NULL;
        }
        printf("%s line #%d\n", args->prefix, i);

        // if someone's waiting for this mutex to be unlocked there's not much that can be done
        must_unlock(&locks[lock_index]);

        lock_index = nextLockIndex(lock_index);
    }
    must_unlock(&locks[lock_index]);

    return NULL;
}


unsigned initMutexes(
        pthread_mutex_t *const mutexes,
        const unsigned count) {
    if (NULL == mutexes) {
        ferrorfln("Unexpected argument value: null");
        return 0;
    }
    pthread_mutexattr_t attributes;
    int errcode = pthread_mutexattr_init(&attributes);
    if (NO_ERROR != errcode) {
        errorfln("Failed to initialize attributes: %s", strerror(errcode));
        return 0;
    }
    errcode = pthread_mutexattr_settype(&attributes, PTHREAD_MUTEX_ERRORCHECK);
    if (NO_ERROR != errcode) {
        errorfln("Failed to set mutex type");
        pthread_mutexattr_destroy(&attributes);
        return 0;
    }
    for (unsigned i = 0; i < count; i += 1) {
        errcode = pthread_mutex_init(&mutexes[i], &attributes);
        if (NO_ERROR != errcode) {
            errorfln("Failed to initialize mutex: %s", strerror(errcode));
            return i;
        }
    }

    errcode = pthread_mutexattr_destroy(&attributes);
    if (NO_ERROR != errcode) {
        errorfln("Failed to destroy mutex attributes: %s", strerror(errcode));
    }

    return count;
}


void destroyMutexes(
        pthread_mutex_t *const mutexes,
        const unsigned count) {
    for (unsigned i = 0; i < count; i += 1) {
        int errcode = pthread_mutex_destroy(&mutexes[i]);
        if (NO_ERROR != errcode) {
            errorfln("Failed to destroy mutex: %s", strerror(errcode));
        }
    }
}


int waitUntilLocked(pthread_mutex_t *const p_lock) {
    const useconds_t WAIT_TIME_NS = 1000;
    const bool cont = true;
    const bool *const p_cont = &cont;
    do {
        int errcode = nanosleep(&(struct timespec) { .tv_nsec = WAIT_TIME_NS }, NULL);
        if (NO_ERROR != errcode && EINTR != errcode) {
            errorfln("Cannot suspend execution: %s", strerror(errno));
        }
        errcode = pthread_mutex_trylock(p_lock);
        if (NO_ERROR == errcode) {
            must_unlock(p_lock);
            continue;
        } else if (EBUSY == errcode) {
            break; // Child locked the mutex
        } else {
            errorfln("Trylock failed: %s", strerror(errcode));
            return EXIT_FAILURE;
        }
    } while (*p_cont);

    return NO_ERROR;
}


int main() {
    int errcode;

    pthread_mutex_t locks[MUTEX_COUNT];

    unsigned initialized = initMutexes(locks, MUTEX_COUNT);
    if (MUTEX_COUNT != initialized) {
        errorfln("Failed to initialize mutexes");
        destroyMutexes(locks, initialized);
        return EXIT_FAILURE;
    }

    errcode = lock(&locks[0]);
    if (NO_ERROR != errcode) {
        errorfln("Cannot lock mutex: %s", strerror(errcode));
        destroyMutexes(locks, MUTEX_COUNT);
        return EXIT_FAILURE;
    }

    const unsigned child_first_lock = 2;

    pthread_t thread;
    errcode = pthread_create(&thread, NULL, printLines, &(PrintArguments_t) {
            .prefix = CHILD_PREFIX,
            .lock_first = child_first_lock,
            .locks = locks
    });
    if (NO_ERROR != errcode) {
        errorfln("Failed to spawn a thread: %s", strerror(errcode));
        return EXIT_FAILURE;
    }

    errcode = waitUntilLocked(&locks[child_first_lock]);
    if (NO_ERROR != errcode) {
        return EXIT_FAILURE;
    }

    printLines(&(PrintArguments_t) {
            .prefix = PARENT_PREFIX,
            .lock_first = DONT_LOCK,
            .locks = locks
    });

    errcode = pthread_join(thread, NULL);
    if (NO_ERROR != errcode) {
        errorfln("Failed to join child thread: %s", strerror(errcode));
        return EXIT_FAILURE;
    }

    destroyMutexes(locks, MUTEX_COUNT);

    return EXIT_SUCCESS;
}


int lock_(
        pthread_mutex_t *p_mutex,
        char const *function,
        int line) {
    int errcode = pthread_mutex_lock(p_mutex);
    if (NO_ERROR != errcode) {
        errorfln("[%s:%d] Cannot lock mutex: %s", function, line, strerror(errcode));
    }
    return errcode;
}


void must_unlock_(
        pthread_mutex_t *const p_mutex,
        char const *const function,
        const int line) {
    int errcode = pthread_mutex_unlock(p_mutex);
    if (NO_ERROR != errcode) {
        errorfln("[FATAL] [%s:%d] Cannot unlock mutex: %s\nExiting", function, line, strerror(errcode));
        exit(EXIT_FAILURE);
    }
}
